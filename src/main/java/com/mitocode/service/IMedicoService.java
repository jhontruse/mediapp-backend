package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.mitocode.model.Medico;

public interface IMedicoService extends ICRUD<Medico>{
	
	Page<Medico> listarPageable(Pageable pageable);
	
}
