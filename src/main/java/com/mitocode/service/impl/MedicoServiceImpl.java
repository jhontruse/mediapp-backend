package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IMedicoDAO;
import com.mitocode.model.Medico;
import com.mitocode.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService{

	@Autowired
	private IMedicoDAO dao;
	
	@Override
	public Medico registrar(Medico t) {	
		return dao.save(t);
	}

	@Override
	public Medico modificar(Medico t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Optional<Medico> listarId(int id) {
		return dao.findById(id);
	}

	@Override
	public List<Medico> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Medico> listarPageable(Pageable pageable) {
		return dao.findAll(pageable);
	}

}
