package com.mitocode.dao.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.mitocode.dao.IPacienteDAONative;
import com.mitocode.model.Paciente;

@Repository
public class PacienteDaoImpl implements IPacienteDAONative {

	final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Paciente> listarAllPacientess() {

		Query nativeQuery = em.createNativeQuery(" SELECT * FROM paciente ");
		List<Paciente> pacientes = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<Object[]> result = nativeQuery.getResultList();

		result.forEach(x -> {
			Paciente paciente = new Paciente();
			paciente.setIdPaciente((Integer.parseInt(String.valueOf(x[0]))));
			paciente.setApellidos(String.valueOf(x[1]));
			paciente.setDireccion(String.valueOf(x[2]));
			paciente.setDni(String.valueOf(x[3]));
			paciente.setEmail(String.valueOf(x[4]));
			paciente.setNombres(String.valueOf(x[5]));
			paciente.setTelefono(String.valueOf(x[6]));

			pacientes.add(paciente);

		});

		return pacientes;
	}

	@Override
	@Transactional
	public Paciente registrarPaciente(Paciente paciente) {
		try {
			logger.info("**************************** registrarPaciente ****************************");
			logger.info(paciente.getApellidos());
			logger.info(paciente.getDireccion());
			logger.info(paciente.getDni());
			logger.info(paciente.getEmail());
			logger.info(paciente.getNombres());
			logger.info(paciente.getTelefono());

			Query query = em.createNativeQuery(
					" INSERT INTO paciente (apellidos,direccion,dni,email,nombres,telefono) VALUES (?,?,?,?,?,?) ");
			query.setParameter(1, paciente.getApellidos());
			query.setParameter(2, paciente.getDireccion());
			query.setParameter(3, paciente.getDni());
			query.setParameter(4, paciente.getEmail());
			query.setParameter(5, paciente.getNombres());
			query.setParameter(6, paciente.getTelefono());
			query.executeUpdate();
		} catch (Exception e) {
			e.getMessage();
			logger.error(e.getMessage());
			paciente = null;
		}
		return paciente != null ? paciente : null;
	}

	@Override
	@Transactional
	public Paciente modificarPaciente(Paciente paciente) {
		try {
			logger.info("**************************** modificarPaciente ****************************");
			logger.info(paciente.getApellidos());
			logger.info(paciente.getDireccion());
			logger.info(paciente.getDni());
			logger.info(paciente.getEmail());
			logger.info(paciente.getNombres());
			logger.info(paciente.getTelefono());

			Query query = em.createNativeQuery(
					" UPDATE paciente SET apellidos = ? , direccion = ? , dni = ? , email = ? , nombres = ? , telefono = ? WHERE id_paciente = ? ");
			query.setParameter(1, paciente.getApellidos());
			query.setParameter(2, paciente.getDireccion());
			query.setParameter(3, paciente.getDni());
			query.setParameter(4, paciente.getEmail());
			query.setParameter(5, paciente.getNombres());
			query.setParameter(6, paciente.getTelefono());
			query.setParameter(7, paciente.getIdPaciente());
			query.executeUpdate();
		} catch (Exception e) {
			e.getMessage();
			logger.error(e.getMessage());
			paciente = null;
		}
		return paciente != null ? paciente : null;
	}

}
