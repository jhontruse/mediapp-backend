package com.mitocode.dao;

import java.util.List;

import com.mitocode.model.Paciente;

public interface IPacienteDAONative {
	
	public List<Paciente> listarAllPacientess();
	
	public Paciente registrarPaciente (Paciente paciente);
	
	public Paciente modificarPaciente (Paciente paciente);

}
