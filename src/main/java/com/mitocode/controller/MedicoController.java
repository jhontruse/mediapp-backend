package com.mitocode.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mitocode.model.Medico;
import com.mitocode.service.IMedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private IMedicoService service;
	
	
	@GetMapping(value="/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Medico>> listarPageable(Pageable pageable){
			Page<Medico> medico;
			medico = service.listarPageable(pageable);
			return new ResponseEntity<Page<Medico>>(medico, HttpStatus.OK);

	}

	@GetMapping(produces = "application/json")
	public List<Medico> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<Medico> listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public Medico registrar(@RequestBody Medico medico) {
		return service.registrar(medico);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public Medico modificar(@RequestBody Medico medico) {
		return service.modificar(medico);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
}
